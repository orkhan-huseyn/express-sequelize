const express = require('express');
const Task = require('./models/task');
const User = require('./models/user');
const sequelize = require('./database');

Task.belongsTo(User, { as: 'user' });

sequelize.sync();

const app = express();

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

app.get('/', async function (req, res) {
  const tasks = await Task.findAll({ include: 'user' });
  res.render('home', {
    tasks,
  });
});

app.get('/create', function (req, res) {
  res.render('create');
});

app.get('/edit/:taskId', async function (req, res) {
  const taskId = req.params.taskId;
  const task = await Task.findByPk(taskId);
  res.render('edit', {
    task,
  });
});

app.post('/todos', async function (req, res) {
  const { title, description, isCompleted } = req.body;
  await Task.create({
    title,
    description,
    isCompleted: Boolean(isCompleted),
    userId: 1
  });
  res.redirect('/');
});

app.post('/todos/:taskId', async function (req, res) {
  const taskId = req.params.taskId;
  const { title, description, isCompleted } = req.body;

  const task = await Task.findByPk(taskId);
  await task.update({
    title,
    description,
    isCompleted: Boolean(isCompleted),
  });

  res.redirect('/');
});

app.get('/delete/:taskId', async function (req, res) {
  const taskId = req.params.taskId;
  const task = await Task.findByPk(taskId);

  task.destroy();

  res.redirect('/');
});

app.listen(8080, function () {
  console.log('🚀 Express app is running on port 8080!');
});
