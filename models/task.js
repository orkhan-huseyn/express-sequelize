const { DataTypes } = require('sequelize');
const sequelize = require('../database');

const Task = sequelize.define(
  'Task',
  {
    title: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
    },
    isCompleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    paranoid: true,
    timestamps: true,
  }
);

module.exports = Task;
