const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
  dialect: 'postgres',
  host: 'localhost',
  username: 'postgres',
  password: 'postgres',
  database: 'todo_app_dev',
});

sequelize
  .authenticate()
  .then(function () {
    console.log('Connected to database successfully!');
  })
  .catch(function (error) {
    console.log('Error connecting to database: ' + error.message);
  });

module.exports = sequelize;
