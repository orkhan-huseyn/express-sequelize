# Nodejs Express & Sequelize Template

Nodejs Express & Sequelize Template with `sequelize` and `express`.

- [sequelize docs](https://sequelize.org/)
- [ejs docs](https://ejs.co/#docs)
- [express docs](https://expressjs.com/en/starter/hello-world.html)
